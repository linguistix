-------------------------------------------------------------
-- Build file: build.lua
-- Consisted in the LaTeX package `linguistix'
-- Copyright © 2022 निरंजन
--
-- This program is free software: you can redistribute it
-- and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your
-- option) any later version.
--
-- This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the
-- implied warranty of MERCHANTABILITY or FITNESS FOR A
-- PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program. If not, see
-- <https://www.gnu.org/licenses/>.
-------------------------------------------------------------
module     = "linguistix"
pkgversion = "0.1-exp"
pkgdate    = os.date("%Y-%m-%d")

-- Tagging:
tagfiles = {"linguistix.dtx", "README.txt", "linguistix.ins"}
function update_tag(file, content, tagname, tagdate)
   if tagname == nil then
      tagname = pkgversion
      tagdate = pkgdate
   end
   if string.match(content,"Version:      v%d+%.%d+%w? %(%d+ %a+, %d+%)\n") then
      content = string.gsub(content,"Version:      v%d+%.%d+%w? %(%d+ %a+, %d+%)\n",
            "Version:      v" .. pkgversion .. " (" .. os.date("%d %B, %Y") .. ")\n")
   end
   if string.match(content,"LaTeX Package linguistix v%d+%.%d+%w?\n") then
      content = string.gsub(content,"LaTeX Package linguistix v%d+%.%d+%w?\n",
            "LaTeX Package linguistix v" .. pkgversion .. "\n")
   end
   return content
end

-- Checking:
checkengines = { "luatex", "xetex" }
stdengine    = "luatex"
checkruns    = 2

-- Documentation:
typesetexe   = "lualatex"
typesetruns  = 2
typesetsuppfiles = { "gfdl-tex.tex" }
typesetfiles = { "linguistix.dtx", "linguistix-example.tex" }
docfiles     = { "COPYING" }